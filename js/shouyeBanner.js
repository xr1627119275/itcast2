$(document).ready(function(e) {
	var unslider04 = $('#playBox').unslider({
		dots: true,
		delay:5e3
	}),
	data04 = unslider04.data('unslider');
	$('.unslider-arrow04').click(function() {
		var fn = this.className.split(' ')[1];
		data04[fn]();
	});
	$(window).resize(function() {
		var ww = $(window).width();
		$('#playBox').css('width', ww);
		$('#playBox ul li').css('width', ww)
	})
});
